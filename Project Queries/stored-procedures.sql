SET sql_mode = ORACLE;

CREATE TABLE Client(

	client_id NUMBER(15) DEFAULT client_seq.nextval, 
	first_name VARCHAR2(100) NOT NULL,
	last_name VARCHAR2(100) NOT NULL,
	PRIMARY KEY(client_id)
	
);

CREATE TABLE Account(
	
	account_id NUMBER(15) DEFAULT account_seq.nextval,
	account_type VARCHAR2(100) NOT NULL,
	balance float(53) NOT NULL,
	PRIMARY KEY(account_id)
	
);

CREATE TABLE ClientAccount(
	
	client_id NUMBER(15) NOT NULL,
	account_id NUMBER(15) NOT NULL,
	CONSTRAINT comp_key PRIMARY KEY(client_id, account_id),
	CONSTRAINT fk_client FOREIGN KEY(client_id) REFERENCES Client(client_id) ON DELETE CASCADE,
	CONSTRAINT fk_account FOREIGN KEY(account_id) REFERENCES Account(account_id) ON DELETE CASCADE

);






DELIMITER //
CREATE OR REPLACE PROCEDURE insert_account(new_client_id in Client.client_id%TYPE, new_account_type in Account.account_type%TYPE , new_balance in Account.balance%TYPE) AS
	v_created_account_id	NUMBER(15);
BEGIN
	INSERT INTO Account(account_type, balance) VALUES (new_account_type, new_balance);
	-- Value value is the primary key of the last account created
	SELECT MAX(account_id) into v_created_account_id FROM Account;
	INSERT INTO ClientAccount(client_id, account_id) VALUES(new_client_id, v_created_account_id);
COMMIT;
END;
//

SELECT * FROM Client;
CALL insert_account(100, 'Savings', 4);

SELECT * FROM ClientAccount;

 Select * FROM Account;

DELETE FROM Account;
Delete FROM ClientAccount;













CREATE or replace SEQUENCE account_seq
	START WITH 100
	INCREMENT BY 1;

CREATE or replace SEQUENCE client_seq
	START WITH 100
	INCREMENT BY 1;


INSERT INTO ClientAccount(client_id, account_id) VALUES (100, 100);

Select * from ClientAccount;


SELECT 
A.account_id, A.account_type, A.balance 
FROM ClientAccount CA
INNER JOIN Account A ON CA.account_id = A.account_id 
INNER JOIN Client C ON CA.client_id = C.client_id
WHERE CA.client_id = 100;

DELIMITER //
CREATE OR REPLACE PROCEDURE get_account_by_id(client_id in ClientAccount.client_id%TYPE)
IS
BEGIN
	SELECT 
	A.account_id, A.account_type, A.balance 
	FROM ClientAccount CA
	INNER JOIN Account A ON CA.account_id = A.account_id 
	INNER JOIN Client C ON CA.client_id = C.client_id
	WHERE CA.client_id = client_id;
	END;
//

CALL get_account_by_id(100);



DELIMITER //
CREATE OR REPLACE PROCEDURE get_account_by_id_with_params(client_id in ClientAccount.client_id%TYPE, amount_greater_than in Account.balance%TYPE, amount_less_than in Account.balance%TYPE)
IS
BEGIN
	SELECT 
	A.account_id, A.account_type, A.balance 
	FROM ClientAccount CA
	INNER JOIN Account A ON CA.account_id = A.account_id 
	INNER JOIN Client C ON CA.client_id = C.client_id
	WHERE CA.client_id = client_id AND A.balance > amount_greater_than AND A.balance < amount_less_than;
	END;
//



CALL get_account_by_id_with_params(100, 2, 4);

SELECT * FROM Account a;

SELECT * FROM ClientAccount ca;

DELETE FROM ClientAccount WHERE account_id = 1128;
DELETE FROm Account WHERE account_id = 1130;



DELIMITER //
CREATE OR REPLACE PROCEDURE get_account_by_id_with_url_params(client_id in ClientAccount.client_id%TYPE, account_id in Account.account_id%TYPE)
IS
BEGIN
	SELECT 
	A.account_id, A.account_type, A.balance 
	FROM ClientAccount CA
	INNER JOIN Account A ON CA.account_id = A.account_id 
	INNER JOIN Client C ON CA.client_id = C.client_id
	WHERE CA.client_id = client_id AND A.account_id = account_id;
	END;
//

get_account_by_id_with_url_params(100, 1131)


DELIMITER //
	CREATE OR REPLACE PROCEDURE update_account_by_id_with_url_params(client_id in ClientAccount.client_id%TYPE, account_id in Account.account_id%TYPE,
	new_account_type in Account.account_type%TYPE, new_balance in Account.balance%TYPE)
	IS
	BEGIN
		UPDATE ClientAccount
		SET
		A.account_type = new_account_type, A.balance = new_balance
		FROM ClientAccount CA
		INNER JOIN Account A ON CA.account_id = A.account_id 
		INNER JOIN Client C ON CA.client_id = C.client_id
		WHERE CA.client_id = client_id AND A.account_id = account_id;
	END;
//
