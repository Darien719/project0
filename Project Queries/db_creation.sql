-- This is a comment in SQL
-- Create Database
-- We surround databases with back ticks ``
CREATE DATABASE `project0_bankdb`;

-- Create a user for mydb
-- Basically everything else is surrounded with ''
CREATE USER 'bankuser' IDENTIFIED BY 'mypassword';

-- DCL GRANT AND REVOKE STATEMENTS - Used to grant and revoke permissions
-- Since we're usign AWS we cant grant all because admin doesnt have all
-- @ means where were granting access, % means all 
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, EXECUTE, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, TRIGGER
ON `project0_bankdb`.* TO 'bankuser'@'%' IDENTIFIED BY 'mypassword' WITH GRANT OPTION;

-- See admin permissions
SHOW GRANTS FOR 'admin'@'%';

-- Commit for permissions
FLUSH PRIVILEGES;