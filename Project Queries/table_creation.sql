SET sql_mode = ORACLE;

CREATE TABLE Client(

	client_id NUMBER(15),
	first_name VARCHAR2(100) NOT NULL,
	last_name VARCHAR2(100) NOT NULL,
	PRIMARY KEY(client_id)
	
);

CREATE TABLE Account(
	
	account_id NUMBER(15),
	account_type VARCHAR2(100) NOT NULL,
	balance float(53) NOT NULL,
	PRIMARY KEY(account_id)
	
);

CREATE TABLE ClientAccount(
	
	client_account_id NUMBER(15),
	client_id NUMBER(15) NOT NULL,
	account_id NUMBER(15) NOT NULL,
	PRIMARY KEY(client_account_id),
	CONSTRAINT fk_client FOREIGN KEY(client_id) REFERENCES Client(client_id) ON DELETE CASCADE,
	CONSTRAINT fk_account FOREIGN KEY(account_id) REFERENCES Account(account_id) ON DELETE CASCADE

);








