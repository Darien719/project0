package com.project0.driver;

import com.project0.controllers.AccountController;
import com.project0.controllers.ClientController;

import io.javalin.Javalin;

public class AppDriver {

	public static void main(String[] args) {

		// Create the javalin webserver and start it on the 9001 port.
		Javalin app = Javalin.create();
		app.start(9001);

		// Client only operations
		// Creates a new client
		app.post("/clients", ClientController.CLIENTPOST);
		// Get all clients
		app.get("/clients", ClientController.CLIENTGET);
		// Get client with the parameters provided in the URL
		app.get("/clients/:client_id", ClientController.CLIENTGETWITHPARAMS);
		// Update client passed in the URL
		app.put("/clients/:client_id", ClientController.CLIENTPUT);
		// Delete client passed in the URL
		app.delete("/clients/:client_id", ClientController.CLIENTDELETE);

		// Account operations
		// Creates a new account for client passed in URL
		app.post("/clients/:client_id/accounts", AccountController.ACCOUNTPOST);
		// Gets all accounts for client passed in URL, also handles query params
		// amountLessThan and amountGreaterThan
		app.get("/clients/:client_id/accounts", AccountController.ACCOUNTGET);
		// Returns specific account_id for client_id based off what is passed in the
		// params
		app.get("/clients/:client_id/accounts/:account_id", AccountController.ACCOUNTGETWITHURLPARAMS);
		//Update specific account_id for client_id based off what is passed in the
		// params 
		app.put("/clients/:client_id/accounts/:account_id", AccountController.ACCOUNTPUT);
		//Delete specific account_id for client_id
		//based off what is passed in the params
		app.delete("/clients/:client_id/accounts/:account_id", AccountController.ACCOUNTDELETE);
		
	}

}
