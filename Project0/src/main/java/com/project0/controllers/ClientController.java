package com.project0.controllers;

import io.javalin.http.Handler;

import java.util.ArrayList;
import java.util.List;

import com.project0.dao.ClientDaoImp;
import com.project0.models.Client;

@SuppressWarnings("unused")
public class ClientController {

	/*
	 * Handles POST Extracts the first_name and last_name from the form parameters.
	 * Passes these to the DAO implementation to create a client. If it was able to
	 * insert into the database it returns a 201 code and prints a success message,
	 * otherwise it returns a 500 server error and returns an error message.
	 */
	public static final Handler CLIENTPOST = (ctx) -> {

		// Failed until we are able to query the database.
		ctx.status(500);

		// Get the form parameters.
		String firstName = ctx.formParam("first_name");
		String lastName = ctx.formParam("last_name");

		int result = ClientDaoImp.insertNewClient(firstName, lastName);
		if (result == 1) {
			System.out.println(
					"Succesfully created client record with first name: " + firstName + " last name: " + lastName);
			ctx.result("Succesfully created client record with first name: " + firstName + " last name: " + lastName);
			ctx.status(201);
		} else {
			System.out.println(
					"Failed to create client record with first name: " + firstName + " last name: " + lastName);
			ctx.result("Failed to create client record with first name: " + firstName + " last name: " + lastName);
			ctx.status(500);
		}

	};

	/*
	 * Handles GET Calls the ClientDAO implementation of getAllClients expecting a
	 * client list. Put the client list in a string that it returns and prints out,
	 * if it was unable to get a list back it returns a 500 error.
	 */
	public static final Handler CLIENTGET = (ctx) -> {
		// Failed until we make query the DB
		ctx.status(500);

		List<Client> clientList = ClientDaoImp.getAllClients();

		// If the list is not empty, we have clients.
		if (clientList.size() > 0) {
			ctx.status(200);
		}

		// Create string with new line character at the end of each client to print out
		String resultString = "";
		for (Client client : clientList) {
			resultString += client.toString();
			resultString += "\n";
		}
		System.out.println(resultString);
		ctx.result(resultString);

	};

	/*
	 * Handles GET with path param client_id Calls the DAO implementation of
	 * getClientById and expects a client object in return. 
	 * If it is able to get a client, it returns a 200 status code.
	 * If it is unable to get a client with that ID, it returns a 404 status code.
	 * If it fails before it queries the DB, it returns a 500 status code.
	 */
	public static final Handler CLIENTGETWITHPARAMS = (ctx) -> {
		// Failed until we query the DB
		ctx.status(500);
		String client_id = ctx.pathParam("client_id");

		Client client = ClientDaoImp.getClientById(client_id);
		//We were able to get a client
		if (client != null) {
			ctx.status(200);
			ctx.result(client.toString());
			System.out.println(client.toString());
		} else { //We were unable to get a client
			ctx.result("No client with that ID exists");
			System.out.println("No client with that ID exist");
			ctx.status(404);
		}

	};

	/*
	 * Handles PUT with path param client_id and form params first_name, last_name
	 * Calls the DAO implementation of updateClients and expects a 1
	 * in return, stating that 1 row was changed. 
	 * If it is able to update a client, it returns a 201 status code. 
	 * If it is unable to update a client with that ID, it returns a 404 status code. 
	 * If it fails before it queries the DB, it returns a 500 status code.
	 */

	public static final Handler CLIENTPUT = (ctx) -> {
		// Failed until we update the client
		ctx.status(500);

		String firstName = ctx.formParam("first_name");
		String lastName = ctx.formParam("last_name");
		String client_id = ctx.pathParam("client_id");

		int result = ClientDaoImp.updateClient(new Client(Integer.parseInt(client_id), firstName, lastName));
		//Updated a client
		if (result == 1) {
			System.out.println("Succesfully updated client " + client_id + "  with first name: " + firstName
					+ " last name: " + lastName);
			ctx.result("Succesfully updated client " + client_id + "  with first name: " + firstName + " last name: "
					+ lastName);
			ctx.status(201);
		} else { //Couldn't update a client
			System.out.println("Failed to update client " + client_id + "  with first name: " + firstName
					+ " last name: " + lastName);
			ctx.result("Failed to update client " + client_id + "  with first name: " + firstName + " last name: "
					+ lastName);
			ctx.status(404);
		}

	};

	/*
	 * Handles DELETE with path param client_id
	 * Calls the DAO implementation of deleteClientById and expects a 1 in return, stating that 1 row was deleted. 
	 * If it is able to delete a client, it returns a 201 status code.
	 * If it is unable to delete a client with that ID it returns a 404 status code. If it fails before it queries the DB it returns
	 * a 500 status code.
	 */
	public static final Handler CLIENTDELETE = (ctx) -> {
		ctx.status(500);

		String client_id = ctx.pathParam("client_id");

		int result = ClientDaoImp.deleteClientById(client_id);
		//Was able to delete a client
		if (result == 1) {
			System.out.println("Succesfully deleted client " + client_id);
			ctx.status(201);
		} else { //Was not able to delete a client
			System.out.println("Failed to delete client " + client_id);
			ctx.status(404);
		}

	};

}
