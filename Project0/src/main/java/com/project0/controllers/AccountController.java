package com.project0.controllers;

import java.util.LinkedList;
import java.util.List;

import com.project0.dao.AccountDaoImp;
import com.project0.models.Account;

import io.javalin.http.Handler;

public class AccountController {

	/*
	 * Handles POST Extracts the client_id from the path parameters and the
	 * account_type and balance from the form. Passes these to the DAO
	 * implementation to create an account. If it was able to insert to the database
	 * it returns a 201 code and prints a success message, otherwise it returns a
	 * 500 server error and returns an error message.
	 */
	public static final Handler ACCOUNTPOST = (ctx) -> {
		// Server error until we are able to finish the operation.
		ctx.status(500);
		// Get the parameters passed in the context
		int clientId = Integer.parseInt(ctx.pathParam("client_id"));
		String accountType = ctx.formParam("account_type");
		double balance = Double.parseDouble(ctx.formParam("balance"));

		// Try the DAO insert operation
		int status = AccountDaoImp.callableInsertAccount(clientId, accountType, balance);

		// Success condition is 3. If it is successful print the account created with a
		// success message.
		if (status == 3) {
			System.out.println("Succesfuly created account for " + clientId + " of type " + accountType
					+ " with balance " + balance);
			ctx.result("Succesfuly created account for " + clientId + " of type " + accountType + " with balance "
					+ balance);
			ctx.status(201);
		} else { // Not successful, say so.
			System.out.println("Unable to create account for " + clientId + " of type " + accountType + " with balance "
					+ balance);
			ctx.result("Unable to create account for " + clientId + " of type " + accountType + " with balance "
					+ balance);
		}

	};

	/*
	 * Handles GET with query params and without it. Extracts the client_id from the
	 * path parameters and then tries to extract amountGreaterThan and
	 * amountLessThan from the path. If it is able to extract it then it is going to
	 * call the implementation for DAO that corresponds to a query with path query
	 * params. If it is not present, it calls a DAO implementation for queries that
	 * only have path params. It expects a List of Accounts that it will print out.
	 * Returns a 500 statue code until it is able to query the database. If it finds
	 * nothing, returns a 404 otherwise returns 200.
	 */
	public static final Handler ACCOUNTGET = (ctx) -> {
		// Server error until we are able to finish the operation.
		ctx.status(500);

		boolean hasQueryParams = true;
		int amountGreaterThan = 0;
		int amountLessthan = 0;

		// Get the parameters passed in the context
		int clientId = Integer.parseInt(ctx.pathParam("client_id"));

		/*
		 * Try to see if there is path query params, if there is there we use the method
		 * getAllAccountsByIdWithParams, else if we use getAllAccountsByid
		 */
		try {
			amountGreaterThan = Integer.parseInt(ctx.queryParam("amountGreaterThan"));
			amountLessthan = Integer.parseInt(ctx.queryParam("amountLessThan"));
		} catch (NumberFormatException e) {
			hasQueryParams = false;
		}

		List<Account> accountListList = new LinkedList<>();

		/*
		 * If it has query params used the query params implementation, else use the
		 * implementation without it
		 */
		if (hasQueryParams) {
			accountListList = AccountDaoImp.getAllAccountsByidWithParams(clientId, amountGreaterThan, amountLessthan);

		} else {
			accountListList = AccountDaoImp.getAllAccountsByid(clientId);
		}

		// If the size is greater than one, we have successfully gotten accounts.
		if (accountListList.size() > 0) {
			ctx.status(200);
		} else { // No accounts found, return 404 and exit the Lambda
			ctx.status(404);
			ctx.result("Client does not exist");
			return;
		}

		// Create the string to return.
		String resultString = "";
		for (Account account : accountListList) {
			resultString += account.toString();
			resultString += "\n";
		}
		System.out.println(resultString);
		ctx.result(resultString);

	};

	/*
	 * Handles GET with multiple path params. Extracts the client_id and account_id
	 * from the path parameters. Tries to do the DAO implementation of getAccounts
	 * with clientId and accountID. If it finds an account it returns the account
	 * with a 200 status code, if it doesn't find any account it returns a 404 code.
	 */
	public static final Handler ACCOUNTGETWITHURLPARAMS = (ctx) -> {
		// Server error until we are able to finish the operation.
		ctx.status(500);
		int clientId = Integer.parseInt(ctx.pathParam("client_id"));
		int accountId = Integer.parseInt(ctx.pathParam("account_id"));

		// Try to get account, if we get back an account object that isn't null, print
		// it out.
		Account account = AccountDaoImp.getAccountByIds(clientId, accountId);
		if (account != null) {
			ctx.status(200);
			ctx.result(account.toString());
			System.out.println(account.toString());
		} else { // No account was returned.
			ctx.result("No account with that ID exists");
			System.out.println("No account with that ID exist");
			ctx.status(404);
		}

	};

	/*
	 * Handles PUT Gets the clientId, accountId from the path and the accountType
	 * and balance from the form. Checks to make sure that the accountId and
	 * clientId belong to the same customer. If it doesn't, it returns a 404 code.
	 * If it does it tries to update the account, if it is successful it returns a
	 * 200 code. If it fails it returns a 500 code.
	 */
	public static final Handler ACCOUNTPUT = (ctx) -> {
		// Server error until we are able to query the server.
		ctx.status(500);

		int clientId = Integer.parseInt(ctx.pathParam("client_id"));
		int accountId = Integer.parseInt(ctx.pathParam("account_id"));
		String accountType = ctx.formParam("account_type");
		double balance = Double.parseDouble(ctx.formParam("balance"));

		/*
		 * Tried to do this all in one query where I join the tables and perform the
		 * update but I could not figure out the syntax for Oracle So we will do it here
		 * in two different queries in which first we make sure that the account belongs
		 * to the client and then we update it if it does.
		 */

		// Check if the account exist and belongs to the client.
		Account account = AccountDaoImp.getAccountByIds(clientId, accountId);
		// The account belongs to the client and it exists, update it.
		if (account != null) {
			int result = AccountDaoImp.updateAccount(accountId, balance, accountType);
			// Success, was able to update 1 row
			if (result == 1) {
				ctx.status(200);
				ctx.result("Succesesfuly updated account" + accountId + " with type: " + accountType + " and balance "
						+ balance);
				System.out.println("Succesesfuly updated account" + accountId + " with type: " + accountType
						+ " and balance " + balance);
			} else { // Failed to update 1 row
				ctx.result("Unsuccesful in updating account" + accountId + " with type: " + accountType
						+ " and balance " + balance);
				System.out.println("Unsuccesful in updating account" + accountId + " with type: " + accountType
						+ " and balance " + balance);
			}

		} else { // The account doesn't belong to the client or doesn't exist
			ctx.result("No account with that ID belongs to this client.");
			System.out.println("No account with that ID belongs to this client.");
			ctx.status(404);
		}

	};

	// Get URL params account_id and client_id and delete the clients account. -
	// return 404 if fails
	public static final Handler ACCOUNTDELETE = (ctx) -> {

		// Server error until we are able to query the server.
		ctx.status(500);

		int clientId = Integer.parseInt(ctx.pathParam("client_id"));
		int accountId = Integer.parseInt(ctx.pathParam("account_id"));

		/*
		 * Tried to do this all in one query where I join the tables and perform the
		 * update but I could not figure out the syntax for Oracle So we will do it here
		 * in two different queries in which first we make sure that the account belongs
		 * to the client and then we update it if it does.
		 */
		
		// Check if the account exist and belongs to the client.
		Account account = AccountDaoImp.getAccountByIds(clientId, accountId);
		// Success, delete
		if (account != null) {

			int result = AccountDaoImp.deleteAccount(accountId);
			// Success, was able to delete 1 row
			if (result == 1) {
				ctx.status(200);
				ctx.result("Succesesfuly delete account " + accountId);
				System.out.println("Succesesfuly delete account " + accountId);
			} else { // Failed to delete 1 row
				ctx.result("Unsuccesful in deleting account " + accountId);
				System.out.println("Unsuccesful in deleting account " + accountId);
			}

		} else { // The account doesn't belong to the client or doesn't exist
			ctx.result("No account with that ID belongs to this client.");
			System.out.println("No account with that ID belongs to this client.");
			ctx.status(404);
		}

	};

}
