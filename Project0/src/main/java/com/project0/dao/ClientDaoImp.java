package com.project0.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.project0.models.Client;

public class ClientDaoImp {

	// Database connection string consist of: jdbc:madiadb://serverhostendpoint:port/dbname
	private static String url = "jdbc:mariadb://revy-sempai.cym6nvtcsn48.us-east-2.rds.amazonaws.com:3306/project0_bankdb";
	private static String username = "bankuser";
	private static String password = "mypassword";

	/*
	 * Creates a new Client entity in the database with firstName, lastName and
	 * balance. Returns how many rows were effected. Should return 1 if it was able
	 * to create 1 client. It was unable, return -1.
	 */
	public static int insertNewClient(String firstName, String lastName) {

		try (Connection con = DriverManager.getConnection(url, username, password)) {

			String sql = "INSERT INTO Client(first_name, last_name) VALUES(?,?)";

			PreparedStatement prepStatement = con.prepareStatement(sql);
			// Have to be specific with the datatype
			prepStatement.setString(1, firstName);
			prepStatement.setString(2, lastName);

			int changed = prepStatement.executeUpdate();

			// If it created a record
			if (changed == 1) {
				return 1;
				// Should only create one record
			} else {
				return -1;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	/*
	 * Query the database to get all Clients.
	 * Turns the result set into a an ArrayList of Client objects and returns it.
	 */
	public static List<Client> getAllClients() {

		List<Client> clientList = new ArrayList<>();

		try (Connection con = DriverManager.getConnection(url, username, password)) {

			String sql = "SELECT * FROM Client";

			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				// rs holds ours client objects
				clientList.add(new Client(rs.getInt(1), rs.getString(2), rs.getString(3)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return clientList;
	}

	/*
	 * Query the database to get a specific client by a specific client_id
	 * Turn the result Set into a Client Object and return it. If it's not able to get any, return null.
	 */
	public static Client getClientById(String id) {

		try (Connection con = DriverManager.getConnection(url, username, password)) {

			String sql = "SELECT * FROM Client WHERE client_id = ?";

			PreparedStatement prepStatement = con.prepareStatement(sql);
			// Have to be specific with the datatype
			prepStatement.setString(1, id);

			ResultSet rs = prepStatement.executeQuery();

			// Get the first one, if I can't then it's not there and return null
			if (rs.next() == false) {
				return null;
			}

			return new Client(rs.getInt(1), rs.getString(2), rs.getString(3));

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;

	}

	/*
	 * Update the database to set the first_name and last_name for a specific client_id.
	 * It is passed to the method through a Client Object.
	 * Returns 1 if it updates a record, else returns -1.
	 */
	public static int updateClient(Client clientToUpdate) {
		try (Connection con = DriverManager.getConnection(url, username, password)) {

			String sql = "UPDATE Client SET first_name = ?, last_name = ? WHERE client_id = ?";

			PreparedStatement prepStatement = con.prepareStatement(sql);
			prepStatement.setString(1, clientToUpdate.getFirstName());
			prepStatement.setString(2, clientToUpdate.getLastName());
			prepStatement.setInt(3, clientToUpdate.getClientId());

			int changed = prepStatement.executeUpdate();
			// If it updated a record
			if (changed == 1) {
				return 1;
				//Could not update a record.
			} else {
				return -1;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;

	}

	/*
	 * Update the database to delete a specific client by client_id.
	 * Returns 1 if it delete
	 *  a record, else returns -1.
	 */
	public static int deleteClientById(String id) {
		try (Connection con = DriverManager.getConnection(url, username, password)) {

			String sql = "DELETE FROM Client WHERE client_id = ?";

			PreparedStatement prepStatement = con.prepareStatement(sql);
			// Have to be specific with the datatype
			prepStatement.setString(1, id);

			int result = prepStatement.executeUpdate();
			// If it delete a record
			if (result == 1) {
				return 1;
				//Could not delete 1 record.
			} else {
				return -1;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return -1;

	}
}
