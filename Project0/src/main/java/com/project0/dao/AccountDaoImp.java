package com.project0.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.project0.models.Account;

public class AccountDaoImp {

	// Connection string makeup = jdbc:madiadb://serverhostendpoint:port/dbname
	private static String url = "jdbc:mariadb://revy-sempai.cym6nvtcsn48.us-east-2.rds.amazonaws.com:3306/project0_bankdb";
	private static String username = "bankuser";
	private static String password = "mypassword";

	/*
	 * Creates a new account entity in the database with clientId, accountType and
	 * balance. Returns how many rows were effected. Should return 3 if it was able
	 * to create 1 account.
	 */
	public static int callableInsertAccount(int clientId, String accountType, double balance) {
		try (Connection con = DriverManager.getConnection(url, username, password)) {

			String sql = "{call insert_account(?,?,?) }";
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, clientId);
			cs.setString(2, accountType);
			cs.setDouble(3, balance);

			int status = cs.executeUpdate();
			return status;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	/*
	 * Query the database to get all account for a specific clientId.
	 * Turns the result set into a an ArrayList of accounts and returns it.
	 */
	public static List<Account> getAllAccountsByid(int clientId) {

		List<Account> accountList = new ArrayList<>();

		try (Connection con = DriverManager.getConnection(url, username, password)) {

			String sql = "{call get_account_by_id(?) }";
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, clientId);

			ResultSet rs = cs.executeQuery();
			while (rs.next()) {
				accountList.add(new Account(rs.getInt(1), rs.getString(2), rs.getDouble(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return accountList;
	}

	/*
	 * Query the database to get all account for a specific clientId in which
	 * the account balance of those account are within amountGreater - amountLessThan, not inclusive.
	 * Turns the result set into a an ArrayList of accounts and returns it.
	 */
	public static List<Account> getAllAccountsByidWithParams(int clientId, int amountGreaterThan, int amountLessThan) {

		List<Account> accountList = new ArrayList<>();

		try (Connection con = DriverManager.getConnection(url, username, password)) {

			String sql = "{call get_account_by_id_with_params(?,?,?) }";
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, clientId);
			cs.setInt(2, amountGreaterThan);
			cs.setInt(3, amountLessThan);

			ResultSet rs = cs.executeQuery();
			while (rs.next()) {
				accountList.add(new Account(rs.getInt(1), rs.getString(2), rs.getDouble(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return accountList;
	}

	/*
	 * Query the database to get a specific account by its specific accountId for a specific clientId.
	 * Turns the result set into an account object and return it.
	 */
	public static Account getAccountByIds(int clientId, int accountId) {

		try (Connection con = DriverManager.getConnection(url, username, password)) {

			String sql = "{call get_account_by_id_with_url_params(?,?) }";

			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, clientId);
			cs.setInt(2, accountId);

			ResultSet rs = cs.executeQuery();

			if (rs.next() == false) {
				return null;
			}
			return new Account(rs.getInt(1), rs.getString(2), rs.getDouble(3));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	/*
	 * Update the database to set the balance and accountType for a specific accountId.
	 * Returns 1 if it updates a record, else returns -1.
	 */
	public static int updateAccount(int accountId, double balance, String accountType) {

		try (Connection con = DriverManager.getConnection(url, username, password)) {

			String sql = "UPDATE Account SET account_type = ?, Balance = ? WHERE account_id = ?";

			PreparedStatement prepStatement = con.prepareStatement(sql);
			prepStatement.setString(1, accountType);
			prepStatement.setDouble(2, balance);
			prepStatement.setInt(3, accountId);

			int changed = prepStatement.executeUpdate();

			// If it updated a record
			if (changed == 1) {
				return 1;
				// Should only update one record
			} else {
				return -1;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	/*
	 * Update the database to delete a specific account by its accountId.
	 * Returns 1 if it deletes a record, else returns -1.
	 */
	public static int deleteAccount(int accountId) {

		try (Connection con = DriverManager.getConnection(url, username, password)) {

			String sql = "DELETE FROM Account WHERE account_id = ?";

			PreparedStatement prepStatement = con.prepareStatement(sql);
			prepStatement.setInt(1, accountId);

			int changed = prepStatement.executeUpdate();

			// If it delete a record
			if (changed == 1) {
				return 1;
				// Should only delete one record
			} else {
				return -1;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

}
